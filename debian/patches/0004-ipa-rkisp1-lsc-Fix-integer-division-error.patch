From: Jacopo Mondi <jacopo.mondi@ideasonboard.com>
Date: Thu, 2 Mar 2023 16:11:45 +0100
Subject: ipa: rkisp1: lsc: Fix integer division error

The RkISP1 implementation of the LensShadingCorrection algorithm has been
made adaptive to the scene color temperature in commit 14c869c00fdd ("ipa:
rkisp1: Take into account color temperature during LSC algorithm").

The LSC algorithm interpolates the correction factors using the
table's reference color temperatures. When calculating the interpolation
coefficients, an unintended integer division makes both coefficient
zeros resulting in a completely black image.

Fix this by type casting to double one of the division operands.

Fixes: 14c869c00fdd ("ipa: rkisp1: Take into account color temperature during LSC algorithm")
Signed-off-by: Jacopo Mondi <jacopo.mondi@ideasonboard.com>
Reviewed-by: Kieran Bingham <kieran.bingham@ideasonboard.com>
Reviewed-by: Paul Elder <paul.elder@ideasonboard.com>
(cherry picked from commit cde9293cf9899b0fc4ce9cf89dd29f26be77f35c)
Signed-off-by: Kieran Bingham <kieran.bingham@ideasonboard.com>
---
 src/ipa/rkisp1/algorithms/lsc.cpp | 4 ++--
 1 file changed, 2 insertions(+), 2 deletions(-)

diff --git a/src/ipa/rkisp1/algorithms/lsc.cpp b/src/ipa/rkisp1/algorithms/lsc.cpp
index 3a443e7..a7ccedb 100644
--- a/src/ipa/rkisp1/algorithms/lsc.cpp
+++ b/src/ipa/rkisp1/algorithms/lsc.cpp
@@ -216,8 +216,8 @@ void LensShadingCorrection::interpolateTable(rkisp1_cif_isp_lsc_config &config,
 					     const Components &set1,
 					     const uint32_t ct)
 {
-	double coeff0 = (set1.ct - ct) / (set1.ct - set0.ct);
-	double coeff1 = (ct - set0.ct) / (set1.ct - set0.ct);
+	double coeff0 = (set1.ct - ct) / static_cast<double>(set1.ct - set0.ct);
+	double coeff1 = (ct - set0.ct) / static_cast<double>(set1.ct - set0.ct);
 
 	for (unsigned int i = 0; i < RKISP1_CIF_ISP_LSC_SAMPLES_MAX; ++i) {
 		for (unsigned int j = 0; j < RKISP1_CIF_ISP_LSC_SAMPLES_MAX; ++j) {
